#!/usr/bin/env python3

# This file is a part of hashedcolls package
# Licensed under Do-What-The-Fuck-You-Want license
# Initially made by @jedi2light (aka Carey Minaieva)

from hashedcolls import HashedDict as d
from hashedcolls import HashedList as l

my = d({d({1: 1}): 1, l([1,2,3]): 2, (1,): 3})

key = d({1: 1})

value = 1

assert my.get(key) == value, 'Test failed (unable to get value)'
