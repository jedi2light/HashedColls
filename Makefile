# This file is a part of hashedcolls package
# Licensed under Do-What-The-Fuck-You-Want license
# Initially made by @jedi2light (aka Carey Minaieva)

.PHONY: tests build clean

PKG = hashedcolls
VER = $(shell python \
        -c 'import hashedcolls; print(hashedcolls.__version_string__)')

SRC_PACKAGE_PATH = './dist/$(PKG)-$(VER).tar.gz'
WHL_PACKAGE_PATH = './dist/$(PKG)-$(VER)-py3-none-any.whl'

tests:
	./runtests.py

build: tests
	python3 -m build

install: ./dist/
	pip3 install $(WHL_PACKAGE_PATH)

clean:
	rm -rf $(PKG).egg-info || true
	rm $(SRC_PACKAGE_PATH) || true
	rm $(WHL_PACKAGE_PATH) || true
	find -type f -name \*.pyc -delete
	find -type d -name __pycache__ -delete

upload: ./dist/
	python3 -m twine upload --repository pypi --verbose $(WHL_PACKAGE_PATH)
